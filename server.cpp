#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <unistd.h>
#include <errno.h>
#include <cstring>
#include <ctime>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h> // for checking if a file exists
#include <netinet/in.h>
#include <arpa/inet.h>
#include <strings.h>

#include <pthread.h>

#include "httpCodes.h"

const unsigned int MAX_CONNECTIONS = 10;
const int on = 1;
bool verbose;

// http responses almost always have a date they were sent with them,
// so just to make web browsers happy, we're including the date
const std::string days[]   = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
const std::string months[] = {"Jan","Feb","Mar","Apr","May","Jun",
                              "Jul","Aug","Sep","Oct","Nov","Dec"};

void sendErrorResponse(int, int, std::string);
std::string currentTime();

void * handleRequest(void * fd)
{
  // "connection" is just the file descriptor
  int sd = *((int*)fd);

  if(verbose)
  {
    for(int i=0; i<40; i++) std::cout << " -";
    std::cout << std::endl;
  }

  int bytesRead = 0;
  int readRetval = 1;
  char buffer[2000];  // we only really need the first line
  bzero(buffer,3000); // 2000 characters is pretty much always enough

  while(bytesRead < 4 ||                 // we don't know how long the
        !(buffer[bytesRead - 4] == '\r'  // header is, so read one byte at a
       && buffer[bytesRead - 3] == '\n'  // time until we see 2 blank lines
       && buffer[bytesRead - 2] == '\r'
       && buffer[bytesRead - 1] == '\n'))
  {
    readRetval = read(sd, &(buffer[bytesRead]), 1);

    if(readRetval == -1)
    {
      // read returned -1, meaning an error
      std::cerr << "Error reading response ("
                << std::strerror(errno) << ")" << std::endl;
      return NULL;
    }

    bytesRead += readRetval;
  }

  std::string r(buffer);
  std::stringstream request(r);

  if(verbose)
  {
    std::cout << r;
    for(int i=0; i<40; i++) std::cout << " -";
    std::cout << std::endl;
  }
  
  std::string requestType;
  std::string path;
  std::string protocol;
  request >> requestType >> path >> protocol;

  if(requestType.compare("GET") != 0
  || path[0] != '/'
  || protocol.compare("HTTP/1.1") != 0)
  {
    // the request is malformed somehow
    sendErrorResponse(sd,HTTP_BAD_REQUEST,"Bad Request");
    close(sd);
    return NULL;
  }

  path = "." + path; // a path might be just the filename or a full path
                     // and paths need ./ (it already starts with /)

  // any path with ../ is dangerous since It might back up
  if(path.find("../") != std::string::npos)
  {
    // paths aren't allowed to back up, respond with HTTP_FORBIDDEN if so
    sendErrorResponse(sd,HTTP_FORBIDDEN,"Forbidden");
    close(sd);
    return NULL;
  }

  // get the file data (this will fail if the file doesn't exist,
  // but not if it exists and we just don't have read permission)
  struct stat fileData;
  if(stat(path.c_str(),&fileData) != 0)
  {
    sendErrorResponse(sd,HTTP_NOT_FOUND,"Not Found");
    close(sd);
    return NULL;
  }

  // I didn't know if you wanted us to deny any file that the server
  // didn't have permission to read, or just "SecretFile.html" specifically
  // so I did both
  if(path.compare("./SecretFile.html") == 0
  || access(path.c_str(), R_OK) != 0)
  {
    sendErrorResponse(sd,HTTP_UNAUTHORIZED,"Unauthorized");
    close(sd);
    return NULL;
  }

  // we should have caught the file not existing before, but just in case
  // check again
  std::ifstream file(path);
  if(!file.is_open())
  {
    sendErrorResponse(sd,HTTP_NOT_FOUND,"Not Found");
    close(sd);
    return NULL;
  }

  // get the size of the file
  const int fileSize = fileData.st_size;
  char fileBuffer[2000];
  
  std::string response = "HTTP/1.1 200 OK\r\n";
  response += "Date: " + currentTime() + "\r\n";
  //response += "Content-type: text/html\r\n";
  response += "Content-length: " + std::to_string(fileSize) +"\r\n";
  response += "Connection: close\r\n";
  response += "\r\n";

  int bytesWritten = 0;
  int messageLength = response.size();
  int writeRetval;

  while(bytesWritten < messageLength)
  {
    writeRetval = write(sd, response.c_str(), messageLength - bytesWritten);
    if(writeRetval == -1)
    {
      // write returned -1, meaning an error
      std::cerr << "Error sending packet header ("
                << std::strerror(errno) << ")" << std::endl;
      //return NULL;
    }
    else
    {
      bytesWritten += writeRetval; 
    }
  }

  if(verbose)
  {
    std::cout << response;
  }

  int totalBytesWritten = 0;
  int writeSize = 0;

  while(totalBytesWritten < fileSize)
  {
    // write it in chunks, in case we're sending a giant file
    // that won't fit in the ram
    writeSize = 2000;
    if(writeSize > fileSize - totalBytesWritten)
    {
      writeSize = fileSize - totalBytesWritten;
    }

    bytesWritten = 0;
    bzero(fileBuffer,2000);
    file.read(fileBuffer,writeSize);
    if(verbose)
    {
      std::cout.write(fileBuffer,writeSize);
    }

    while(bytesWritten < writeSize)
    {
      writeRetval = write(sd, &(fileBuffer[bytesWritten]), 
                          writeSize - bytesWritten);

      if(writeRetval == -1)
      {
        // read returned -1, meaning an error
        std::cerr << "Error sending packet ("
                  << std::strerror(errno) << ")" << std::endl;
        return NULL;
      }
      else
      {
        bytesWritten += writeRetval;
      }
    }

    totalBytesWritten += bytesWritten;
  }

  if(verbose)
  {
    for(int i=0; i<80; i++) std::cout << "-";
    std::cout << std::endl;
  }

  // we need to close the connection, but when sending a large file,
  // the last part of it gets lost, so set a timer for a bit (0.5 sec)
  usleep(500 * 1000);
  close(sd);

  return NULL;
}

int main(int argc, char** argv)
{
  // the uw1-320-xx remote machines (and my own laptop)
  // need root permission to listen to port 80
  // so I added the option to use a different port
  int port = 80;
  bool defaultPort = true;
  verbose = false;

  for(int i=0; i<argc; i++)
  {
    if(strcmp(argv[i],"-P") == 0 || strcmp(argv[i],"--port") == 0)
    {
      if(argc >= i)
      {
        port = atoi(argv[i+1]);
        defaultPort = false;
        if(port != 80)
        std::cout << "Note: using port " << port 
                  << " instead of 80" << std::endl;
        i++;
      }
      else
      {
        std::cerr << "Error: --port needs an argument" << std::endl;
        exit(EXIT_FAILURE);
      }
    }
    if(strcmp(argv[i],"-v") == 0 || strcmp(argv[i],"--verbose") == 0)
    {
      verbose = true;
    }
  }

  if(defaultPort)
  {
    std::cout << "Attempting to bind to port 80" << std::endl;
    std::cout << "You can use another port with -P ## or --port ##"<< std::endl;
  }

  pthread_t thread;

  // declare a new socket address
  sockaddr_in acceptSocketAddress;
  // zero it out
  bzero( (char*)&acceptSocketAddress, sizeof(acceptSocketAddress));

  // set some member variables
  acceptSocketAddress.sin_family      = AF_INET;
  acceptSocketAddress.sin_addr.s_addr = htonl(INADDR_ANY);
  acceptSocketAddress.sin_port        = htons(port);

  // open socket with inet
  int serverSid = socket( AF_INET, SOCK_STREAM, 0);

  // set SO_REUSEADDR (so the port is released when the program closes)
  setsockopt(serverSid, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(int)); 

  // bind socket to local address
  int bindRet = bind(serverSid, (sockaddr*)&acceptSocketAddress, 
                     sizeof(acceptSocketAddress));
  if(bindRet == -1)
  {
    std::cout << "failed to bind socket ("
              << std::strerror(errno) << ")" << std::endl;
    exit(EXIT_FAILURE);
  }

  // listen to the socket
  listen(serverSid, MAX_CONNECTIONS);

  // receive a request
  while(true)
  {
    // make a new socket address
    sockaddr_in newSocketAddress;
    socklen_t newSocketAddressSize = sizeof(newSocketAddress);

    // wait for a client to connect (and get a file desc. of the socket)
    int fd = accept(serverSid, (sockaddr*)&newSocketAddress, 
                    &newSocketAddressSize);
    if(verbose)
    {
      std::cout << "New connection" << std::endl;
    }

    // handle the request in a new thread
    pthread_create(&thread, NULL, handleRequest, (void*)&fd);
  }
}

void sendErrorResponse(int sd, int code, std::string message)
{
  std::string responseBody = 
    "<!DOCTYPE html><html><head><title>Error</title></head>";
  responseBody += "<body><div style=\"";
  responseBody += "text-align: center;";
  responseBody += "border: 3px solid red;";
  responseBody += "width: 50%;";
  responseBody += "margin: auto;";
  responseBody += "font-size: 28px;";
  responseBody += "font-weight: bold;";
  responseBody += "\">" + std::to_string(code) + " " + message;
  responseBody += "</div></body></html>";

  std::string response = "HTTP/1.1 ";
  response += std::to_string(code) + " ";
  response += message + "\r\n";
  response += "Date: " + currentTime() + "\r\n";
  response += "Content-type: text/html\r\n";
  response += "Content-length: " + std::to_string(responseBody.size()) +"\r\n";
  response += "Connection: close\r\n";
  response += "\r\n";
  response += responseBody;

  int bytesWritten = 0;
  int messageLength = response.size();
  int writeRetval;

  while(bytesWritten < messageLength)
  {
    writeRetval = write(sd, response.c_str(), messageLength - bytesWritten);
    if(writeRetval == -1)
    {
      // write returned -1, meaning an error
      std::cerr << "Error sending packet (errno = "
                << errno << ")" << std::endl;
      return;
    }
    bytesWritten += writeRetval; 
  }
}

std::string currentTime()
{
  time_t now;
  time(&now);
  struct tm* ct = gmtime(&now);

  std::string retval = days[ct->tm_wday];
  retval += ((ct->tm_mday < 10 ? ", 0" : ", ")
            + std::to_string((ct->tm_mday))) + " ";
  retval += months[ct->tm_mon] + " ";
  retval += std::to_string(1900 + ct->tm_year) + " ";
  retval += (ct->tm_hour < 10 ? "0" : "") + std::to_string(ct->tm_hour) + ":";
  retval += (ct->tm_min  < 10 ? "0" : "") + std::to_string(ct->tm_min ) + ":";
  retval += (ct->tm_sec  < 10 ? "0" : "") + std::to_string(ct->tm_sec )
         + " GMT";

  return retval;
}
