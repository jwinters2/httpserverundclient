#!/bin/sh

# run the server with "./server -P 3559"

echo "--------------------------------"
echo " 2) ACCESSING A REAL WEB SERVER "
echo "--------------------------------"
# this is the first website I found that doesn't 301 redirect
# to the https version
./client www.scp-wiki.net scp-096 -o outFile2
echo -e "\n"
echo "--------------------------------------"
echo " 3) ACCESSING FILE FROM MY WEB SERVER "
echo "--------------------------------------"
./client localhost index.html -o outFile3 -P 3559
echo -e "\n"
echo "---------------------------------------------------"
echo " 4) ACCESSING UNAUTHORIZED FILE FROM MY WEB SERVER "
echo "---------------------------------------------------"
./client localhost SecretFile.html -o outFile4 -P 3559
echo -e "\n"
echo "------------------------------------------------"
echo " 5) ACCESSING FORBIDDEN FILE FROM MY WEB SERVER "
echo "------------------------------------------------"
./client localhost ../../../foo.txt -o outFile5 -P 3559
echo -e "\n"
echo "---------------------------------------------------"
echo " 6) ACCESSING NON-EXISTENT FILE FROM MY WEB SERVER "
echo "---------------------------------------------------"
./client localhost DNE.txt -o outFile6 -P 3559
echo -e "\n"
echo "---------------------------------------------------"
echo " 7) ACCESSING NON-EXISTENT FILE FROM MY WEB SERVER "
echo "---------------------------------------------------"
# I added the -m option to read a packet from cin, instead of making it itself
echo -ne "jsdk;lfjsjdf;l\r\n\r\n" | ./client localhost asdf --manual -o outFile7 -P 3559
echo -e "\n"

# tests 2 and 3 should make files, check if they exist
if [ ! -f outFile2 ]; then
  echo "Error, Test 2 doesn't make a file even though it should"
else
  rm outFile2
fi

if [ ! -f outFile3 ]; then
  echo "Error, Test 3 doesn't make a file even though it should"
else
  rm outFile3
fi

# tests 4 - 7 shouldn't make files, check if they exist
if [ -f outFile4 ]; then
  echo "Error, Test 4 makes a file even though it shouldn't"
  rm outFile4
fi

if [ -f outFile5 ]; then
  echo "Error, Test 5 makes a file even though it shouldn't"
  rm outFile5
fi

if [ -f outFile6 ]; then
  echo "Error, Test 6 makes a file even though it shouldn't"
  rm outFile6
fi

if [ -f outFile7 ]; then
  echo "Error, Test 7 makes a file even though it shouldn't"
  rm outFile7
fi

