#!/bin/sh

if [ "$1" != "-s" ]; then
  echo "building client" &&
  g++ --std=c++11 -Wall client.cpp -o client
fi

if [ "$1" != "-c" ]; then
  echo "building server" &&
  g++ --std=c++11 -Wall server.cpp -pthread -o server
fi

echo "done"
