#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <unistd.h>
#include <errno.h>
#include <cstring>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>

#include "httpCodes.h"

const int BATCH_SIZE = 2000;

bool sendGET(int, const char*, const char*, bool, bool);
bool saveResponse(int, const char*, const char*, bool, bool);
bool readChunks(int, std::ofstream&, bool);

int hexStringToInt(std::string s)
{
  int retval = 0;
  for(unsigned int i=0; i<s.size(); i++)
  {
    if(s[i] >= '0' && s[i] <= '9')
    {
      retval = (retval * 0x10) + (s[i] - '0');
    }
    if(s[i] >= 'a' && s[i] <= 'f')
    {
      retval = (retval * 0x10) + (s[i] - 'a' + 10);
    }
    if(s[i] >= 'A' && s[i] <= 'F')
    {
      retval = (retval * 0x10) + (s[i] - 'A' + 10);
    }
  }

  return retval;
}

std::string toLowerCase(std::string s)
{
  std::string retval = "";
  for(unsigned int i=0; i<s.size(); i++)
  {
    if(s[i] >= 'A' && s[i] <= 'Z')
    {
      retval += (s[i] + 'a' - 'A');
    }
    else
    {
      retval += s[i];
    }
  }

  return retval;
}

std::string parseFilename(std::string s)
{
  std::string retval = "";
  for(unsigned int i=0; i<s.size(); i++)
  {
    if(s[i] == '/')
    {
      retval = "";
    }
    else
    {
      retval += s[i];
    }
  }
  return retval;
}

int main(int argc, char** argv)
{
  // we need 2 arguments, quit if we don't get that
  if(argc < 2 + 1)
  {
    std::cerr << "Error: not enough arguments" << std::endl
              << "takes the following arguments:" << std::endl
              << "1: server address" << std::endl
              << "2: file to retrieve" << std::endl
              << "optional arguments:" << std::endl
              << "    -o, --output-file -- output-file" << std::endl
              << "    -q, --quiet -- quiet mode (don't print contents to cout)"
              << std::endl
              << "    -p, --packet -- show packets sent and recieved"
              << "    -P, --port -- port to use (80 by default)" << std::endl
              << std::endl
              << "    -m, --manual -- read packet directly from cin"
              << std::endl;
    exit(EXIT_FAILURE);
  }

  // read command line arguments
  const char* serverName = argv[1];
  const char* reqFileName = argv[2];
  char* saveFileName = argv[2];
  int port = 80;
  bool quiet = false;
  bool packet = false;
  bool manual = false;
  for(int i=3; i<argc; i++)
  {
    if(strcmp(argv[i],"-o") == 0 || strcmp(argv[i],"--output-file") == 0)
    {
      if(i+1 >= argc || argv[i+1][0] == '-')
      {
        std::cerr << "Error: -o needs an argument (filename to save to)"
                  << std::endl;
        exit(EXIT_FAILURE);
      }
      else
      {
        saveFileName = argv[i+1];
        i++;
      }
    }
    else if(strcmp(argv[i],"-q") == 0 || strcmp(argv[i],"--quiet") == 0)
    {
      quiet = true; 
    }
    else if(strcmp(argv[i],"-p") == 0 || strcmp(argv[i],"--packet") == 0)
    {
      packet = true; 
    }
    else if(strcmp(argv[i],"-m") == 0 || strcmp(argv[i],"--manual") == 0)
    {
      manual = true; 
    }
    else if(strcmp(argv[i],"-P") == 0 || strcmp(argv[i],"--port") == 0)
    {
      if(i+1 >= argc || argv[i+1][0] == '-')
      {
        std::cerr << "Error: -P needs an argument (port to use)" << std::endl;
        exit(EXIT_FAILURE);
      }
      else
      {
        port = atoi(argv[i+1]);
        i++;
      }
    }
  }

  // make a hostent to resolve the server name
  struct hostent* host = gethostbyname(serverName);

  // declare a new socket address
  sockaddr_in sendSocketAddress;
  // zero it out
  bzero( (char*)&sendSocketAddress, sizeof(sendSocketAddress));

  // check if host has been resolved properly
  if(host == 0)
  {
    // the server address doesn't exist
    std::cerr << "Error: cannot resolve hostname (" << serverName << ")"
              << std::endl;
    exit(EXIT_FAILURE);
  }

  // setup the socket address
  sendSocketAddress.sin_family      = AF_INET;
  sendSocketAddress.sin_addr.s_addr =
      inet_addr( inet_ntoa (*(struct in_addr*)*host->h_addr_list) );
  sendSocketAddress.sin_port        = htons(port);

  // open socket (quit if it fails)
  int clientSid = socket( AF_INET, SOCK_STREAM, 0);
  if(clientSid == -1)
  {
    std::cerr << "Could not open a socket" << std::endl; 
    exit(EXIT_FAILURE);
  }

  // connect the socket to the server
  if(connect(clientSid, (sockaddr*)&sendSocketAddress, 
             sizeof(sendSocketAddress)) != 0)
  {
    std::cerr << "Could not connect to server [" << serverName << ":"
              << port << "]" << std::endl;
    exit(EXIT_FAILURE);
  }

  // make and send GET request
  sendGET(clientSid, serverName, reqFileName, packet, manual);

  std::string sfn(saveFileName);
  if(sfn.back() == '/')
  {
    // if this is a directory, save the file in the directory, but
    // use the original filename
    sfn.append(parseFilename(std::string(reqFileName)));
  }

  saveResponse(clientSid, serverName,sfn.c_str(),quiet,packet);

  // close the socket
  close(clientSid);
}

bool sendGET(int sd, const char* serverName,
             const char* fileName, bool showPackets, bool manualInput)
{
  std::string request; 
  if(manualInput)
  {
    std::cout << "--- Enter request (blank line to end) --------" << std::endl;
    std::string line;
    do
    {
      std::getline(std::cin,line);  // read line by line
      line += "\r\n";               // add line break
      request += line;              // add current line to request
    }
    while(line.compare("\r\n") != 0); // request is done when blank line
  }
  else
  {
    request += "GET /";
    request += fileName;
    request += " HTTP/1.1\r\n";
    request += "Host: ";
    request += serverName;
    request += "\r\n";
    //request += "Accept: text/plain,text/html\r\n";
    request += "Connection: close\r\n";
    request += "\r\n";
  }

  int bytesWritten = 0;
  int messageLength = request.size();
  int writeRetval;

  while(bytesWritten < messageLength)
  {
    writeRetval = write(sd, request.c_str(), messageLength - bytesWritten);
    if(writeRetval == -1)
    {
      // write returned -1, meaning an error
      std::cerr << "Error sending packet ("
                << std::strerror(errno) << ")" << std::endl;
      return false;
    }
    bytesWritten += writeRetval; 
  }

  if(showPackets)
  {
    std::cout <<
    "-----------------------------------------------------" << std::endl
              << "BEGIN SENT PACKET" << std::endl
              << request << "END SENT PACKET" << std::endl <<
    "-----------------------------------------------------" << std::endl;
  }

  return true;
}

bool saveResponse(int sd, const char* serverName,
                  const char* fileName, bool quiet, bool showPackets)
{
  int bytesRead = 0;
  int readRetval = 1;
  char buffer[2000];  // the google says that response headers are
  bzero(buffer,2000); // usually less than 2KB

  while(bytesRead < 4 ||                 // we don't know how long the
        !(buffer[bytesRead - 4] == '\r'  // header is, so read one byte at a
       && buffer[bytesRead - 3] == '\n'  // time until we see a blank line
       && buffer[bytesRead - 2] == '\r'  // (two newlines in a row)
       && buffer[bytesRead - 1] == '\n'))
  {
    readRetval = read(sd, &(buffer[bytesRead]), 1);

    if(readRetval == -1)
    {
      // read returned -1, meaning an error
      std::cerr << "Error reading response ("
                << std::strerror(errno) << ")" << std::endl;
      return false;
    }

    bytesRead += readRetval;
  }

  // convert the buffer into a string stream to make it easier to work with
  std::string h(buffer);
  std::stringstream header(h);

  if(showPackets)
  {
    std::cout << "BEGIN RECEIVED PACKET HEADER" << std::endl
              << h << "END RECEIVED PACKET HEADER" << std::endl <<
    "-----------------------------------------------------" << std::endl;
  }

  // get the response code
  int responseCode;
  char responseMessage[256];
  header.ignore(256,' '); // skip the first word (it's just HTTP/1.1)
  header >> responseCode; // get the response code
  header.ignore(256,' '); // skip the space
  header.getline(responseMessage, 256, '\r'); // get the rest of the line

  // determine the length of the response
  std::string headerField;
  int contentLength = 0;
  bool isChunked = false;

  // read the header fields that we care about
  while(header.good())
  {
    // get the first word of the line
    header >> headerField;
    headerField = toLowerCase(headerField); // html is not case-sensitive

    // if it's something we care about, get the data
    if(headerField.compare("content-length:") == 0)
    {
      header >> contentLength;
    }
    if(headerField.compare("transfer-encoding:") == 0)
    {
      header >> headerField;
      isChunked = (headerField.compare("chunked") == 0);
    }

    // ignore the rest of the line
    header.ignore(256,'\n');
  }

  std::string fn(fileName);
  std::ofstream outFile;
  
  // only write to a file if we get a 200 OK response
  if(responseCode == HTTP_OK)
  {
    /* first try to copy the server's path
     * i.e. getting www.server.com/foo/bar/baz.txt
     * will save to ./foo/bar/baz.txt
     *
     * or saving to ./school/networking/hw2/baz.txt
     * will preserve the path
     */
    outFile.open("./" + fn); 

    if(!outFile.is_open())
    {
      // if that fails, just get the actual file name and save as ./baz.txt
      outFile.open("./" + parseFilename(fn));
      if(!outFile.is_open())
      {
        // if that fails too, return an error
        std::cerr << "Error: cannot open file " << fileName << std::endl;
        return false;
      }
      else
      {
        // warn the user that the path they gave us didn't work
        std::cerr << "Warning: invalid path, saving to current directory"
                  << std::endl;
      }
    }
  }
    
  // if the response is in chunks, handle it accordingly
  if(isChunked)
  {
    return readChunks(sd,outFile,quiet);
  }

  // read into the file
  int totalBytesRead = 0;
  int readSize = 0;
  char contentBuffer[BATCH_SIZE];

  while(totalBytesRead < contentLength)
  {
    // read it in chunks, in case we're retrieving a giant file
    // that won't fit in the ram
    readSize = BATCH_SIZE;
    if(readSize > contentLength - totalBytesRead)
    {
      readSize = contentLength - totalBytesRead;
    }
    bytesRead = 0;

    while(bytesRead < readSize)
    {
      readRetval = read(sd, &(contentBuffer[bytesRead]), readSize - bytesRead);

      if(readRetval == -1)
      {
        // read returned -1, meaning an error
        std::cerr << "Error reading response (errno = "
                  << errno << ")" << std::endl;
        return false;
      }

      bytesRead += readRetval;
    }

    if(outFile.is_open()) outFile.write(contentBuffer,readSize);
    if(!quiet) std::cout.write(contentBuffer,readSize);
    totalBytesRead += bytesRead;
  }

  outFile.close();

  return true;
}

bool readChunks(int sd, std::ofstream& outFile, bool quiet)
{
  std::string chunkSizeStr = "";
  int chunkSize = 0;

  // this just needs to store the size (in hex)
  // there's no way it's going to be bigger than 256
  char sizeBuffer[256];
  int bytesRead = 0;

  // chunks end when the "next chunk size" is 0
  while(chunkSizeStr.compare("0\r\n") != 0)
  {
    bytesRead = 0;
    bzero(sizeBuffer,256);

    // get the size of the next chunk
    while(bytesRead < 2 ||
          !(sizeBuffer[bytesRead - 2] == '\r'
         && sizeBuffer[bytesRead - 1] == '\n'))
    {
      bytesRead += read(sd, &(sizeBuffer[bytesRead]), 1);
    }

    chunkSizeStr.assign(sizeBuffer);
    chunkSize = hexStringToInt(chunkSizeStr);

    // read the next chunkSize bytes from the input
    char chunkBuffer[chunkSize];
    bytesRead = 0;
    while(bytesRead < chunkSize)
    {
      bytesRead += read(sd, &(chunkBuffer[bytesRead]), chunkSize - bytesRead);
    }

    if(outFile.is_open()) outFile.write(chunkBuffer,chunkSize);
    if(!quiet) std::cout.write(chunkBuffer,chunkSize);
  }

  outFile.close();
  return true;
}
